<?php

/**
* @file
* Code to migrate legacy resources.
*/

abstract class ResourceMigration extends Migration {
  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();

    // With migrate_ui enabled, migration pages will indicate people involved in
    // the particular migration, with their role and contact info. We default the
    // list in the shared class; it can be overridden for specific migrations.
    $this->team = array(
      new MigrateTeamMember('Lisa Sawin', 'lisa@lisasawin.com', t('Developer, iFactory')),
      );
  }
}

class ResourceNodeMigration extends ResourceMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Importing existing NU Library table of external sites to Drupal Resource nodes.');

    $this->map = new MigrateSQLMap($this->machineName,
    array(
    'external_sites_id' => array(
      'type' => 'int',
      'not null' => TRUE,
      'description' => 'External Sites ID.',
      'alias' => 'r',
      )
      ),
      MigrateDestinationNode::getKeySchema()
      );

    // We have a more complicated query. The Migration class fundamentally
    // depends on taking a single source row and turning it into a single
    // Drupal object, so how do we deal with zero or more terms attached to
    // each node? One way (demonstrated for MySQL) is to pull them into a single
    // comma-separated list.
    $query = db_select('external_sites', 'r')
      ->fields('r', 
      array('external_sites_id', 
      'name', 
      'url', 
      'availability', 
      'moreinfo', 
      'notes',
      'gloss', 
      'licensed_db',
      'name_to_subject',
      'name_to_type'));

    $this->source = new MigrateSourceSQL($query);

    // Set up our destination - nodes of type migrate_example_beer
    $this->destination = new MigrateDestinationNode('resource');

    // EXTERNAL SITES      RESOURCE
    // external_sites_id   nothing
    // name                title
    // url                 field_resource_link
    // availability        field_resource_access
    // moreinfo            field_resource_help_link
    // notes               field_resource_announcement
    // gloss               field_resource_description
    // licensed_db         field_resource_licensed_db
    // name_to_subject     field_resource_subject
    // name_to_type        field_resource_type

    // Mapped fields
    // Title
    $this->addFieldMapping('title', 'name')
      ->description(t('Mapping external sites name in source to node title'));
    // Resource link
    $this->addFieldMapping('field_resource_link', 'url')
      ->description(t('Mapping external sites url in source to node field_resource_link'));
    // Availability
    $this->addFieldMapping('field_resource_access', 'availability')
      ->description(t('Mapping external sites availability in source to node field_resource_access'));
    // Help link
    $this->addFieldMapping('field_resource_help_link', 'moreinfo')
      ->description(t('Mapping external sites moreinfo in source to node field_resource_help_link'));
    // Announcement
    $this->addFieldMapping('field_resource_announcement', 'notes')
      ->description(t('Mapping external sites notes in source to node field_resource_announcement'));
    // Description
    $this->addFieldMapping('field_resource_description', 'gloss')
         ->description(t('Mapping external sites gloss in source to node field_resource_description'))
         ->arguments(array('format' => 'filtered_html'));
    // Licensed db
    $this->addFieldMapping('field_resource_licensed_db', 'licensed_db')
      ->description(t('Mapping external sites licensed_db source to node field_resource_licensed_db'));
    // Type
    $this->addFieldMapping('field_resource_type', 'name_to_type')
      ->description(t('Mapping external sites name_to_type source to node field_resource_type'));
    // Subject
    $this->addFieldMapping('field_resource_subject', 'name_to_subject')
      ->description(t('Mapping external sites name_to_subject source to node field_resource_subject'));


  }

  public function prepare(stdClass $resource, stdClass $row) {
    if ($row->licensed_db == 'Y') {
      $resource->field_resource_licensed_db['und'][0]['value'] = 1;
    } 
    if ($row->licensed_db == 'N') {
      $resource->field_resource_licensed_db['und'][0]['value'] = 0;
    }
    $resource->field_resource_link      = _set_link_field($resource->field_resource_link, $row->url);
    $resource->field_resource_help_link = _set_link_field($resource->field_resource_help_link, $row->moreinfo);
  } 
  
}